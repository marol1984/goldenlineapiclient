<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 15:03
 */

namespace Goldenline;



class Request
{

    protected $requestObject;
    protected $endpoint;
    protected $creationTime;

    public function __construct($endpoint)
    {
        $this->setEndpoint( $endpoint );
        $this->setCreationTime( time() );
    }


    /**
     * @return mixed
     */
    public function getRequestObject()
    {
        return $this->requestObject;
    }

    /**
     * @param mixed $requestObject
     * @return Request
     */
    public function setRequestObject($requestObject)
    {
        $this->requestObject = $requestObject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param mixed $endpoint
     * @return Request
     */
    protected function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreationTime()
    {
        return $this->creationTime;
    }

    /**
     * @param mixed $creationTime
     * @return Request
     */
    protected function setCreationTime($creationTime)
    {
        $this->creationTime = $creationTime;
        return $this;
    }




}