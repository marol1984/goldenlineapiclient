<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-21
 * Time: 10:42
 */

namespace Goldenline;


class Response
{

    protected $status;
    protected $statusCode;
    protected $responseObject;

    public static function createFromJson($jsonResponseBody){
        $responseBody = json_decode($jsonResponseBody);

        if($responseBody === null){
            throw new Exception("INVALID JSON RESPONSE");
        }

        $response = new self();
        $response->setResponseObject($responseBody);
        $response->setStatus($responseBody->result);

        return $response;

    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Response
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return Response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseObject()
    {
        return $this->responseObject;
    }

    /**
     * @param mixed $responseObject
     * @return Response
     */
    public function setResponseObject($responseObject)
    {
        $this->responseObject = $responseObject;
        return $this;
    }





}