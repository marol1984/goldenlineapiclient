<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 13:51
 */

namespace Goldenline;

use Goldenline\Authentication\AuthenticableInterface;
use Goldenline\Authentication\Credentials;
use Goldenline\Request\AuthenticatedRequest;

class Client
{

    const API_ENDPOINT_URL = "https://panel.goldenline.pl";

    protected $credentials;

    /**
     * @return Credentials
     */
    public function getCredentials() : Authentication\Credentials
    {
        return $this->credentials;
    }

    /**
     * @param Credentials $credentials
     * @return Client
     */
    public function setCredentials(Authentication\Credentials $credentials)
    {
        $this->credentials = $credentials;
        return $this;
    }

    public function call(Request $request){


        if($request instanceof AuthenticableInterface){
            $request->authenticate( $this->getCredentials() );
            $requestUri = self::API_ENDPOINT_URL . $request->getAuthenticatedEndpoint();
        } else {
            $requestUri = self::API_ENDPOINT_URL . $request->getEndpoint();
        }

        $httpClient = new \GuzzleHttp\Client();
        $res = $httpClient->request( $request->getMethod(), $requestUri, $this->makeRequestBody());
        return Response::createFromJson( $res->getBody() )->setStatusCode($res->getStatusCode());

    }

    protected function makeRequestBody(Request $request){
        return ["form_params"=>["json"=>json_encode($request->getRequestObject())]];
    }

}