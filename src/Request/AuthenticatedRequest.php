<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 15:17
 */

namespace Goldenline\Request;

use Goldenline\Authentication\AuthenticableInterface;
use Goldenline\Request;
use Goldenline\Authentication\Sign;

class AuthenticatedRequest extends Request implements AuthenticableInterface{

    protected $authenticatedEndpoint;


    public function authenticate(\Goldenline\Authentication\Credentials $credentials)
    {

        $authenticationParams["ts"] = $this->getCreationTime();
        $authenticationParams["app_key"] = $credentials->getAppKey();
        $authenticationParams["sign"] = Sign::generate($credentials->getAppKey(), $credentials->getSecret(), $this->getEndpoint(), $this->getCreationTime());
        $this->setAuthenticatedEndpoint($this->getEndpoint() . "?" . http_build_query($authenticationParams));



    }

    /**
     * @param mixed $authenticatedEndpoint
     */
    protected function setAuthenticatedEndpoint($authenticatedEndpoint)
    {
        $this->authenticatedEndpoint = $authenticatedEndpoint;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthenticatedEndpoint()
    {
        return $this->authenticatedEndpoint;
    }


}