<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-21
 * Time: 08:26
 */

namespace Goldenline\Request\Get;

use Goldenline\Request\AuthenticatedRequest;
use Goldenline\Request\RequestInterface;

class Regions extends AuthenticatedRequest implements RequestInterface {

    public function __construct()
    {
        parent::__construct(self::ENDPOINT_REGIONS);
    }

    public function getMethod()
    {
        return self::METHOD_GET;
    }

}