<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-21
 * Time: 08:26
 */

namespace Goldenline\Request\Post;

use Goldenline\Request\AuthenticatedRequest;
use Goldenline\Request\RequestInterface;

class OfferStatus extends AuthenticatedRequest implements RequestInterface {

    public function __construct()
    {
        parent::__construct(self::ENDPOINT_OFFER_STATUS);
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

}