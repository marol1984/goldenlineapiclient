<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-21
 * Time: 08:45
 */

namespace Goldenline\Request;



interface RequestInterface
{

    const METHOD_GET = "get";
    const METHOD_POST = "post";
    const METHOD_PUT = "put";

    const ENDPOINT_BRANCHES = "/ofapi/others/branches/";
    const ENDPOINT_SPECIALITIES = "/ofapi/others/specialities/";
    const ENDPOINT_REGIONS = "/ofapi/others/regions/";

    const ENDPOINT_OFFER_EDIT = "/ofapi/offer/edit/";
    const ENDPOINT_OFFER_STATUS = "/ofapi/offer/status/";
    const ENDPOINT_OFFER_CHANGE_STATUS = "/ofapi/offer/change_status/";
    const ENDPOINT_OFFER_STATISTICS = "/ofapi/offer/statistics/";


    public function getEndpoint();
    public function getMethod();
    public function getRequestObject();



}