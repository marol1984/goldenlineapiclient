<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 14:37
 */

namespace Goldenline\Authentication;


class Sign
{
    public static function generate($appKey, $secretKey, $endpoint, $ts){
        return hash("sha256", $appKey . $secretKey . $endpoint . $ts);
    }
}