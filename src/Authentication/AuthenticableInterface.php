<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 15:10
 */

namespace Goldenline\Authentication;

interface AuthenticableInterface
{
    public function authenticate(\Goldenline\Authentication\Credentials $credentials);
}