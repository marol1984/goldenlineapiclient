<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2018-11-20
 * Time: 14:53
 */

namespace Goldenline\Authentication;


class Credentials
{

    protected $appKey;
    protected $secret;

    public static function create($appKey, $secret){
        $credentials = new self();
        $credentials->setAppKey($appKey);
        $credentials->setSecret($secret);
        return $credentials;
    }

    /**
     * @return mixed
     */
    public function getAppKey()
    {
        return $this->appKey;
    }

    /**
     * @param mixed $appKey
     * @return Credentials
     */
    public function setAppKey($appKey)
    {
        $this->appKey = $appKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     * @return Credentials
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }



}