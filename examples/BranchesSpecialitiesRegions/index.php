<?php

use Goldenline\Authentication\Credentials;
use Goldenline\Client;

define("APP_KEY", "3e4ebb50a61b70da1f88239be55e7f72");
define("SECRET", "19ac4e005be78ac86203f53ddf169cb8");
define("HASH_KEY", "fd677e49274b9fafb483d80bd3b6e398");

require(dirname(dirname(__DIR__)) . "/autoload.php");

$client = new Client();

$response = $client->setCredentials( Credentials::create(APP_KEY,SECRET) )->call( new \Goldenline\Request\Get\Specialities() );
$response = $client->setCredentials( Credentials::create(APP_KEY,SECRET) )->call( new \Goldenline\Request\Get\Regions() );
$response = $client->setCredentials( Credentials::create(APP_KEY,SECRET) )->call( new \Goldenline\Request\Get\Branches() );

header("Content-type: application/json");
echo json_encode($response->getResponseObject());